
import {shoeArr} from '../../data'
import { ADD_TO_CART, CHANGE_QUANTITY, REMOVE_CART } from '../types/ShoeShopTypes';

let intialState = {
    shoeArr : shoeArr,
    gioHang : [],
}
export let shoeShopReducer = (state=intialState,{type,payload,step}) => {
    switch (type){

        case ADD_TO_CART :{
            let index = state.gioHang.findIndex((item) => item.id == payload.id);
            let cloneGioHang = [...state.gioHang];
            if (index == -1) {
                let newSp = { ...payload, soLuong: 1 };
                cloneGioHang.push(newSp);
            } else {
                cloneGioHang[index].soLuong++;
            }
            state.gioHang = cloneGioHang;
            return {...state} 
        }
        case REMOVE_CART : {
             let cloneGioHang = [...state.gioHang];
            cloneGioHang = cloneGioHang.filter(item => item.id !== payload)
            state.gioHang = cloneGioHang
            return {...state}
        }
        case CHANGE_QUANTITY : {
            let index = state.gioHang.findIndex((item) => {
                return (item.id == payload);
              });
              let cloneGioHang = [...state.gioHang];
              cloneGioHang[index].soLuong  += step;
              if(cloneGioHang[index].soLuong == 0){
                cloneGioHang.splice(index,1)
              }
              state.gioHang = cloneGioHang;
              return {...state}
        }
        default : 
        return state;
    }
}