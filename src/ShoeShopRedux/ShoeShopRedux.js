import React, { Component } from 'react'
import ListShoe from './ListShoe'
import TableGioHang from './TableGioHang'

export default class ShoeShopRedux extends Component {
  render() {
    return (
      <div className='container'>
        <TableGioHang/>
        <ListShoe/>
      </div>
    )
  }
}
