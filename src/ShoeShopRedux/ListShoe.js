import React, { Component } from 'react'
import {connect} from 'react-redux'
import ItemShoe from './ItemShoe'

 class ListShoe extends Component {
    renderShoe = () => { 
    return this.props.shoeArr.map((item, index) => { 
     return  <ItemShoe key={index} shoeData={item}/>
     })

     }
  render() {
    return (
      <div className='row'>{this.renderShoe()}</div>
    )
  }
}
let mapStateToProps = (state) => { 
    return{
        shoeArr : state.shoeShopReducer.shoeArr
    }
 }
 export default connect(mapStateToProps)(ListShoe);
