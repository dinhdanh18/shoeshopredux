import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CHANGE_QUANTITY, REMOVE_CART } from './redux/types/ShoeShopTypes'

class TableGioHang extends Component {   
    renderContent = () => { 
        return this.props.cart.map((item) => { 
            return <tr key={item.id}>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.price}</td>
            <td> <button onClick={() => { 
                this.props.handleChangeQuantity(item.id,-1)
            }}  className="btn btn-secondary">-</button>
            <span className="mx-2"> {item.soLuong}</span>
            <button onClick={() => {
                this.props.handleChangeQuantity(item.id,1)
            }} className="btn btn-warning">+</button></td>
            <td><button className='btn btn-danger' onClick={() => {
                this.props.handleRemoveCart(item.id)
            }}>Xóa</button></td>
        </tr>
         })
    }
  render() {
    return (
        <div>
        <table className='table'>
            <thead >
                <tr >
                    <th>Id</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            {this.renderContent()}
            </tbody>
        </table>
      </div>
    )
  }
}

let mapStateToProps = (state) =>{
    return {
        cart : state.shoeShopReducer.gioHang,
    }
}
let mapDispatchToProps = (dispatch) => { 
    return{
        handleRemoveCart : (idShoe) => { 
            dispatch({
                type: REMOVE_CART,
                payload: idShoe,
            })
         },
         handleChangeQuantity : (idShoe,step) =>{
            dispatch({
                type: CHANGE_QUANTITY,
                payload: idShoe,
                step: step,
            })
         }
    }
  }
export default connect(mapStateToProps,mapDispatchToProps)(TableGioHang)